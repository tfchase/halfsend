#ifndef _INTAKE_H_
#define _INTAKE_H_

#include "api.h"

#ifdef __cplusplus
extern "C" {

extern pros::Mutex intake_mutex;



#define LEFT_ROLLER_MOTOR_PORT 11
#define RIGHT_ROLLER_MOTOR_PORT 3

extern pros::Motor left_roller_mtr;
extern pros::Motor right_roller_mtr;

void intakeControlTaskInit();
void intakeMove(int sideRollerPower);

#endif
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
/**
 * You can add C++-only headers here
 */

#endif

#endif  // _INTAKE_H_
