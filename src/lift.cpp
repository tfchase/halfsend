#include "main.h"
#include "base.h"
#include "lift.h"

pros::Motor left_lift_mtr(LEFT_LIFT_MOTOR_PORT);
pros::Motor right_lift_mtr(RIGHT_LIFT_MOTOR_PORT, REVERSE);

pros::Mutex lift_mutex;
pros::Mutex lift_toggle_mutex;

int lift_toggle;
int lift_up;
int lift_down;

void liftToggleControl(void* param) {

  std::uint32_t now = pros::millis();
  bool lift_is_down = true;

  while (true) {
    if (lift_toggle_mutex.take(MUTEX_WAIT_SHORT)) {
      lift_toggle = master.get_digital_new_press(DIGITAL_X);

      if (lift_toggle && lift_is_down) {
        lift_up = true;
        lift_down = false;
        lift_is_down = false;
      } else if (lift_toggle && !lift_is_down) {
        lift_up = false;
        lift_down = true;
        lift_is_down = true;
      }

      lift_toggle_mutex.give();
    }
    pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
  }
}


void liftControl(void* param) {
  //manage lift
  std::uint32_t now = pros::millis();
  double currentPos = (left_lift_mtr.get_position() + right_lift_mtr.get_position())/2;
  double topTargetPos = 2200;
  double null = 50;

  while (true) {
    if (lift_mutex.take(MUTEX_WAIT_SHORT)) {
      if (lift_up) {
        currentPos = (left_lift_mtr.get_position() + right_lift_mtr.get_position())/2;
          while (currentPos < topTargetPos && lift_up) {
            liftMove(127);
            currentPos = (left_lift_mtr.get_position() + right_lift_mtr.get_position())/2;
            pros::delay(20);
          }
          liftMove(0);
          lift_up = false;
      } else if (lift_down) {
        currentPos = (left_lift_mtr.get_position() + right_lift_mtr.get_position())/2;
          while (currentPos > null && lift_down) {
            liftMove(-127);
            currentPos = (left_lift_mtr.get_position() + right_lift_mtr.get_position())/2;
            pros::delay(20);
          }
          liftMove(0);
          lift_down = false;
      }
      lift_mutex.give();
    }
    pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
  }
}

void liftMove(int power) {
  right_lift_mtr = power;
  left_lift_mtr = power;
  right_lift_mtr.set_brake_mode((pros::E_MOTOR_BRAKE_HOLD));
  left_lift_mtr.set_brake_mode((pros::E_MOTOR_BRAKE_HOLD));
}


void liftControlTaskInit() {
  pros::Task lift_task(liftControl,(void*)"DUMMY");
  pros::Task lift_toggle(liftToggleControl, (void*)"DUMMY");
}
