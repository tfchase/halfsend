#include "main.h"
#include "base.h"

pros::Mutex base_mutex;

pros::Motor right_front_mtr(FRONT_RIGHT_DRIVE_PORT, REVERSE);
pros::Motor left_front_mtr(FRONT_LEFT_DRIVE_PORT);
pros::Motor right_back_mtr(BACK_RIGHT_DRIVE_PORT, REVERSE);
pros::Motor left_back_mtr(BACK_LEFT_DRIVE_PORT);

void baseControl(void* param) {
  //manage base
  std::uint32_t now = pros::millis();
  right_front_mtr.set_current_limit(DEFAULT_CURRENT);
	left_front_mtr.set_current_limit(DEFAULT_CURRENT);
	right_back_mtr.set_current_limit(DEFAULT_CURRENT);
	left_back_mtr.set_current_limit(DEFAULT_CURRENT);
  while (true) {
    if (base_mutex.take(MUTEX_WAIT_SHORT)) {
      int left = master.get_analog(ANALOG_LEFT_Y);
      int right = master.get_analog(ANALOG_RIGHT_Y);

      left_back_mtr = left;
      right_back_mtr = right;
      left_front_mtr = left;
      right_front_mtr = right;

			base_mutex.give();
    }
    pros::Task::delay_until(&now, TASK_DELAY_NORMAL);
  }
}

void baseControlTaskInit() {
  pros::Task base_task(baseControl,(void*)"DUMMY");
}
